use std::env;
use std::error::Error;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::os::unix::net::{UnixListener, UnixStream};
use std::path::Path;
use std::result::Result;
use std::thread;

static COLOR_OK: &'static str = "green";
static COLOR_WARN: &'static str = "orange";
static COLOR_ERR: &'static str = "red";

extern "C" {
    fn get_nprocs() -> u32;
}

fn calc_battery_level_color(current_level: u8) -> &'static str {
    match current_level {
        l if l > 50 => COLOR_OK,
        l if l > 25 => COLOR_WARN,
        _ => COLOR_ERR,
    }
}

fn calc_power_usage_color(usage: f32) -> &'static str {
    match usage {
        u if u < 10.0 => COLOR_OK,
        u if u < 20.0 => COLOR_WARN,
        _ => COLOR_ERR,
    }
}

fn calc_cpu_color(usage_percent: f32) -> &'static str {
    match usage_percent {
        u if u < 0.25 => COLOR_OK,
        u if u < 0.75 => COLOR_WARN,
        _ => COLOR_ERR,
    }
}

fn calc_disk_color(size_total: u64, size_current: u64) -> &'static str {
    match size_current as f32 / size_total as f32 {
        u if u < 0.75 => COLOR_OK,
        u if u < 0.9 => COLOR_WARN,
        _ => COLOR_ERR,
    }
}

fn calc_load_color(load: f64, num_cpus: u32) -> &'static str {
    match load {
        l if l < 1.0 => COLOR_OK,
        l if l < num_cpus.into() => COLOR_WARN,
        _ => COLOR_ERR,
    }
}

fn calc_mem_color(total: u64, used: u64) -> &'static str {
    let percent = used as f64 / total as f64;
    match percent {
        p if p < 0.5 => COLOR_OK,
        p if p < 0.85 => COLOR_WARN,
        _ => COLOR_ERR,
    }
}

fn bytes_to_gb(bytes: u64) -> f64 {
    bytes as f64 / (1024.0 * 1024.0 * 1024.0)
}

struct BatteryInfo {
    charge_level_current: Option<u8>,
    charge_level_max: Option<u8>,
    power_usage: Option<f32>,
}

struct CpuInfo {
    idle_time: chrono::Duration,
    total_time: chrono::Duration,
}

struct DiskInfo {
    size_current: u64,
    size_total: u64,
}

struct LoadInfo {
    load: (f64, f64, f64),
}

struct MemInfo {
    total: u64,
    free: u64,
}

impl BatteryInfo {
    fn new(
        charge_level_current: Option<u8>,
        charge_level_max: Option<u8>,
        power_usage: Option<f32>,
    ) -> BatteryInfo {
        BatteryInfo {
            charge_level_current: charge_level_current,
            charge_level_max: charge_level_max,
            power_usage: power_usage,
        }
    }

    fn print_colorized(self) -> bool {
        let mut output = String::new();
        if let Some(charge_level_current) = self.charge_level_current {
            let battery_level_color = calc_battery_level_color(charge_level_current);
            let charge_level_max = self.charge_level_max;
            if let Some(charge_level_max) = charge_level_max {
                output += &format!(
                    "#[fg={}]{}#[fg=default]/#[fg={}]{}%",
                    battery_level_color,
                    charge_level_current,
                    battery_level_color,
                    charge_level_max,
                );
            } else {
                output += &format!("#[fg={}]{}%", battery_level_color, charge_level_current);
            }
        }

        if let Some(power_usage) = self.power_usage {
            if !output.is_empty() {
                output.push(' ');
            }

            output += &format!(
                "#[fg={}]{:.1} W",
                calc_power_usage_color(power_usage),
                power_usage
            );
        }

        if output.is_empty() {
            false
        } else {
            print!("{}", output);
            true
        }
    }
}

impl CpuInfo {
    fn new(idle_time: chrono::Duration, total_time: chrono::Duration) -> CpuInfo {
        CpuInfo {
            idle_time: idle_time,
            total_time: total_time,
        }
    }

    fn print_colorized(self) {
        let cpu_usage_percent = (self.total_time.num_milliseconds() as f32
            - self.idle_time.num_milliseconds() as f32)
            / self.total_time.num_milliseconds() as f32;
        let cpu_usage_color = calc_cpu_color(cpu_usage_percent);
        print!("#[fg={}]{:.0}%", cpu_usage_color, cpu_usage_percent * 100.0);
    }
}

impl DiskInfo {
    fn new(size_total: u64, size_current: u64) -> DiskInfo {
        DiskInfo {
            size_current: size_current,
            size_total: size_total,
        }
    }

    fn print_colorized(self) {
        let disk_usage_color = calc_disk_color(self.size_total, self.size_current);
        let size_current_gb = bytes_to_gb(self.size_current);
        let size_total_gb = bytes_to_gb(self.size_total);
        let precision = if size_total_gb - size_current_gb > 10.0 {
            0
        } else {
            1
        };
        print!(
            "#[fg={}]{:.*}/{:.*} GB",
            disk_usage_color, precision, size_current_gb, precision, size_total_gb
        );
    }
}

impl LoadInfo {
    fn new(load: (f64, f64, f64)) -> LoadInfo {
        LoadInfo { load: load }
    }

    fn print_colorized(self) {
        let num_cpus = unsafe { get_nprocs() };
        print!(
            "#[fg={}]{:.2} #[fg={}]{:.2} #[fg={}]{:.2}",
            calc_load_color(self.load.0, num_cpus),
            self.load.0,
            calc_load_color(self.load.1, num_cpus),
            self.load.1,
            calc_load_color(self.load.2, num_cpus),
            self.load.2,
        );
    }
}

impl MemInfo {
    fn new(total: u64, free: u64) -> MemInfo {
        MemInfo {
            total: total,
            free: free,
        }
    }

    fn print_colorized(self) {
        let used = self.total - self.free;
        let used_gb = bytes_to_gb(used);
        print!(
            "#[fg={}]{:.*}/{:.1} GB",
            calc_mem_color(self.total, used),
            if used_gb < 0.1 { 0 } else { 1 },
            used_gb,
            bytes_to_gb(self.total)
        );
    }
}

fn proc_meminfo() -> Result<MemInfo, Box<dyn Error>> {
    let file = File::open("/proc/meminfo")?;
    let reader = BufReader::new(file);

    let mut mem_total = 0u64;
    let mut mem_free = 0u64;
    let mut buffers = 0u64;
    let mut cached = 0u64;
    let mut s_reclaimable = 0u64;
    let mut num_found = 0;
    for line in reader.lines() {
        let line = line?;
        if !line.ends_with(" kB") {
            continue;
        }

        let subline = line.trim_end_matches(" kB");

        let split = subline.split_once(':');
        if let Some(split) = split {
            match split.0 {
                "MemTotal" => {
                    mem_total = split.1.trim().parse()?;
                    mem_total *= 1024;
                    num_found += 1;
                    if num_found == 5 {
                        break;
                    }
                }
                "MemFree" => {
                    mem_free = split.1.trim().parse()?;
                    mem_free *= 1024;
                    num_found += 1;
                    if num_found == 5 {
                        break;
                    }
                }
                "Buffers" => {
                    buffers = split.1.trim().parse()?;
                    buffers *= 1024;
                    num_found += 1;
                    if num_found == 5 {
                        break;
                    }
                }
                "Cached" => {
                    cached = split.1.trim().parse()?;
                    cached *= 1024;
                    num_found += 1;
                    if num_found == 5 {
                        break;
                    }
                }
                "SReclaimable" => {
                    s_reclaimable = split.1.trim().parse()?;
                    s_reclaimable *= 1024;
                    num_found += 1;
                    if num_found == 5 {
                        break;
                    }
                }
                _ => {}
            }
        }
    }

    if num_found != 5 {
        return Err(std::boxed::Box::new(std::io::Error::new(
            std::io::ErrorKind::Other,
            "Failed to find all memory types",
        )));
    }

    Ok(MemInfo::new(
        mem_total,
        mem_free + buffers + cached + s_reclaimable,
    ))
}

fn maybe_proc<T: std::str::FromStr>(path: &str) -> Option<T> {
    match std::fs::read_to_string(path) {
        Ok(val_str) => match val_str.trim().parse::<T>() {
            Ok(val) => Some(val),
            Err(_) => None,
        },
        Err(_) => None,
    }
}

fn proc_battery_info() -> Result<BatteryInfo, Box<dyn Error>> {
    let capacity: Option<u8> = maybe_proc("/sys/class/power_supply/BAT0/capacity");
    let charge_level_max: Option<u8> =
        maybe_proc("/sys/class/power_supply/BAT0/charge_control_end_threshold");
    let power_now: Option<u32> = maybe_proc("/sys/class/power_supply/BAT0/power_now");
    let power_usage_watts =
        power_now.and_then(|power_now| Some(power_now as f32 / (1024.0 * 1024.0)));

    Ok(BatteryInfo::new(
        capacity,
        charge_level_max,
        power_usage_watts,
    ))
}

// https://man7.org/linux/man-pages/man5/proc.5.html
// user nice system idle iowait irq softirq steal guest guest_nice
// 0    1    2      3    4      5   6       7     8     9
fn read_proc_stat() -> Result<Vec<i32>, Box<dyn Error>> {
    let file = File::open("/proc/stat")?;
    let mut reader = BufReader::new(file);

    let mut line = String::new();
    reader.read_line(&mut line)?;
    if !line.starts_with("cpu ") {
        return Err(std::boxed::Box::new(std::io::Error::new(
            std::io::ErrorKind::Other,
            "Failed to find cpu in /proc/stat",
        )));
    }
    let line = line.trim_start_matches("cpu ").trim();
    let nums: Vec<i32> = line
        .split(" ")
        .map(|num| num.parse().unwrap_or(-1))
        .collect();
    if nums.contains(&-1) {
        return Err(std::boxed::Box::new(std::io::Error::new(
            std::io::ErrorKind::Other,
            "Failed to parse cpu stats",
        )));
    }

    Ok(nums)
}

fn proc_cpu_info() -> Result<CpuInfo, Box<dyn Error>> {
    let prev = read_proc_stat()?;
    std::thread::sleep(std::time::Duration::from_secs(1));
    let curr = read_proc_stat()?;

    let prev_idle = prev[3] + prev[4];
    let curr_idle = curr[3] + curr[4];

    let prev_non_idle = prev[0] + prev[1] + prev[2] + prev[5] + prev[6] + prev[7];
    let curr_non_idle = curr[0] + curr[1] + curr[2] + curr[5] + curr[6] + curr[7];

    let prev_total = prev_idle + prev_non_idle;
    let curr_total = curr_idle + curr_non_idle;

    let total_diff = curr_total - prev_total;
    let idle_diff = curr_idle - prev_idle;

    let clock_tick = nix::unistd::sysconf(nix::unistd::SysconfVar::CLK_TCK)?.unwrap_or(100);

    let total_us = total_diff as i64 * 1000 * 1000 / clock_tick;
    let idle_us = idle_diff as i64 * 1000 * 1000 / clock_tick;

    Ok(CpuInfo::new(
        chrono::Duration::microseconds(idle_us),
        chrono::Duration::microseconds(total_us),
    ))
}

fn proc_disk_info() -> Result<DiskInfo, Box<dyn Error>> {
    let stat = nix::sys::statvfs::statvfs("/")?;
    let size_total = stat.blocks() * stat.fragment_size();
    let size_current = size_total - stat.blocks_free() * stat.fragment_size();

    Ok(DiskInfo::new(size_total, size_current))
}

fn print_status() -> Result<(), Box<dyn Error>> {
    let info = nix::sys::sysinfo::sysinfo()?;
    let ram_info = proc_meminfo()?;
    ram_info.print_colorized();

    print!(" #[fg=default]:: ");
    let swap_info = MemInfo::new(info.swap_total(), info.swap_free());
    swap_info.print_colorized();

    print!(" #[fg=default]:: ");
    let load_info = LoadInfo::new(info.load_average());
    load_info.print_colorized();

    print!(" #[fg=default]:: ");
    let cpu_info = proc_cpu_info()?;
    cpu_info.print_colorized();

    print!(" #[fg=default]:: ");
    let disk_info = proc_disk_info()?;
    disk_info.print_colorized();

    print!(" #[fg=default]:: ");
    let battery_info = proc_battery_info()?;
    let battery_info_printed = battery_info.print_colorized();

    let now = chrono::Local::now();
    println!(
        "{}#[fg=default]{}",
        if battery_info_printed { " " } else { "" },
        now.format("%b %d %H:%M")
    );

    Ok(())
}

fn handle_client(mut stream: UnixStream) {
    stream.write_all(b"hello world").unwrap();
}

fn run_daemon(socket_path: &str) -> Result<(), Box<dyn Error>> {
    if let Err(err) = std::fs::remove_file(socket_path) {
        let os_err = err.raw_os_error().unwrap_or(-1);
        if os_err != 2 {
            return Err(Box::new(err));
        }
    }

    let listener = UnixListener::bind(socket_path)?;

    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                thread::spawn(|| handle_client(stream));
            }
            Err(err) => {
                println!("Stream failed: {}", err);
            }
        }
    }

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() == 2 && args[1] == "--daemon" {
        let runtime_dir = env::var("XDG_RUNTIME_DIR").unwrap();
        let socket_path = Path::new(&runtime_dir).join("tmux-status.socket");
        println!("Running in daemon mode on {}", socket_path.display());
        run_daemon(socket_path.to_str().unwrap()).unwrap();
    } else {
        print_status().unwrap();
    }
}
